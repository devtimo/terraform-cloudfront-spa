variable "aliases" {
  description = "Lista de alias para a distribuição do cloudfront"
  type = list(string)
}

variable "bucket_origin" {
  description = "Nome do bucket da origin"
}

variable "bucket_log" {
  description = "Nome do bucket de log da origin"
}

variable "s3_origin_id" {
  description = "Id de origin da S3 para o cloudfront"
  default = "S3 Origin"
}

variable "viewer_protocol_policy" {
  description = "Tipo de protocolos permitidos. Possíveis valores: 'allow-all', 'https-only', 'redirect-to-https'"
  default = "redirect-to-https"
}

variable "acm_certificate_arn" {
  description = "ARN do certificado SSL/TLS do ACM."
}



