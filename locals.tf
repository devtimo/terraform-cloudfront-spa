locals {
  hosted_zone_dash = replace(var.aliases[0], ".", "-")
  resource_comment = "${local.hosted_zone_dash}-origin"
}
