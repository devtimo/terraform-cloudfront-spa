
output "hosted_zone_id" {
  value = aws_cloudfront_distribution.origin.hosted_zone_id
}

output "domain_name" {
  value = aws_cloudfront_distribution.origin.domain_name
}
